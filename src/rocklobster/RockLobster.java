package rocklobster;

import java.util.ArrayList;
import info.gridworld.grid.*;
import info.gridworld.actor.*;

/**
 * A Rock Lobster is a critter that has special moving instructions 
 * that are too long to list here. :D <br/>
 * AP Computer Science Project 3 Part 2
 * @author Brian Yang
 */
public class RockLobster extends Critter {
    
    /**
     * Processes the element of actors. 
     * "Eats" any flowers or bugs and turns rocks into rock lobsters. 
     * @post (1) The state of all actors in the grid other than this critter and the elements of actors 
     * is unchanged and the elements of actors are unchanged. (2) The location of this 
     * critter is unchanged.
     * @param actors all neighboring actors to be processed
     */
    @Override
    public void processActors(ArrayList<Actor> actors) {
        for (Actor a : actors) {
            if ((a instanceof Bug) || (a instanceof Flower))
                // remove bugs and flowers
                a.removeSelfFromGrid();
            else if (a instanceof Rock) 
                // place a new Rock Lobster in the rock's place
                (new RockLobster()).putSelfInGrid(getGrid(), a.getLocation());
        }
    }
    
    /**
     * Get a list of possible locations for the next move. <br/>
     * Implemented to return the two forward-diagonal locations and directly forward. <br/>
     * @post The state of all actors is unchanged
     * @return list of possible valid unoccupied move locations
     */
    @Override
    public ArrayList<Location> getMoveLocations() {
        ArrayList<Location> locations = new ArrayList<Location>();
        Grid<Actor> gr = getGrid();
        
        // Add Front Left Location
        Location fLeft = getLocation().getAdjacentLocation(getDirection() + Location.HALF_LEFT);
        if (gr.isValid(fLeft)) { // if its a valid location
            if(gr.get(fLeft) == null) // then is it occupied?
                locations.add(fLeft);
        }
        
        // Add Front Right Location
        Location fRight = getLocation().getAdjacentLocation(getDirection() + Location.HALF_RIGHT);
        if (gr.isValid(fRight)) { // if its a valid location
            if(gr.get(fRight) == null) // then is it occupied?
                locations.add(fRight);
        }
        
        // Neither of the two diagonal locations are valid, so how about forward?
        if (locations.isEmpty()) {
            Location ahead = getLocation().getAdjacentLocation(getDirection()); // can it move forward then
                if (gr.isValid(ahead) && gr.get(ahead) == null)
                    locations.add(ahead);
        }
        return locations;
    }
    
    /**
     * Selects a location for the next move. <br/>
     * Will select the location that has the most adjacent neighbors.
     * @post (1) The returned location is an element of locs, this critter's current location, or null.
     * (2) The state of all actors is unchanged
     * @param locs a list of valid and unoccupied move locations
     * @return the location selected
     */
    @Override
    public Location selectMoveLocation(ArrayList<Location> locs) {
        if (locs.isEmpty()) // if no valid locations, then return own location
            return getLocation();
        
        if (locs.size() == 1) // only one move location, so return that
            return locs.get(0);
        
        Grid<Actor> gr = getGrid();
        
        Location fLeft = locs.get(0);
        Location fRight = locs.get(1);
        
        // number of objects adjacent to each spot
        int fLeftAdjacent = gr.getOccupiedAdjacentLocations(fLeft).size();
        int fRightAdjacent = gr.getOccupiedAdjacentLocations(fRight).size();
        
        if (fLeftAdjacent == fRightAdjacent) // if both locations have the same number of neighbors
            return locs.get((int)(Math.random() * locs.size()));
        else if (fLeftAdjacent > fRightAdjacent) // left is more crowded
            return fLeft;
        else if (fLeftAdjacent < fRightAdjacent) // right is more crowded
            return fRight;
        else // fail...
            return null;
    }
    
    /**
     * Moves the critter to the given location or remove if the location is null. <br/>
     * If the given location is its current location, then turn around instead.
     * @post (1) getLocation() == loc , (2) The states of all actors other than 
     * those at the new and old locations are unchanged
     * @param loc the location to move to
     */
    @Override
    public void makeMove(Location loc) {
        if (loc == null)
            removeSelfFromGrid();
        else if (loc.equals(getLocation()))
            setDirection(getDirection() + Location.HALF_CIRCLE);
        else
            moveTo(loc);
    }
}
