package rocklobster;

import info.gridworld.actor.ActorWorld; 
import info.gridworld.grid.Location; 
import java.awt.Color;
import info.gridworld.actor.Rock;

/**
 * Tester for RockLobster
 * @author Brian Yang
 */
public class RockLobsterTester {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ActorWorld world = new ActorWorld(); // make the world!
        
        RockLobster panther = new RockLobster(); // construct new dancing bug
        panther.setColor(Color.MAGENTA); // set color of my dancing bug
        
        Rock rock = new Rock();
        
        world.add(new Location(7,8), panther); // add the dancing bug to the world
        world.add(new Location(8,9), rock);
        world.show(); // show the world!
    }
}
